const express = require('express');
const mongoose = require('mongoose');
require('dotenv/config');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');

// Local host test

app.get('/', (req, res) => {
    res.send('Recrutement Game API');
});

//  middlewares

app.use(bodyParser.json());
app.use(cors());

//short cut for User router

const userssRouter = require('./routes/users');
app.use('/users', userssRouter);

// connect to dbb

mongoose.connect(process.env.DB_CONNECTION,
    {useNewUrlParser: true, useUnifiedTopology: true}, () => console.log('Connected to server'));

// listen 

app.listen(3000);