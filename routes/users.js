const express = require('express');
const router = express.Router();
const User = require('../moduls/User');

// Create a user 
router.post('/create', async (req, res) => {
    const user = new User({
        name: req.body.name,
        role: req.body.role,
        age: req.body.age,
        date: req.body.date 
    })
    try {
        const savedUser = await user.save();
        res.json(savedUser);
    } catch (err) {
        res.json({message: err});
    }
})

// Find all users

router.get('/findAllUsers', async (req, res) =>{
    try {
        const users = await User.find();
        res.json(users);
    } catch (err) {
        res.json({message: err});
    }
})

// Find user by ID

router.get('/:userId', async (req, res) =>{
    try {
        const user = await User.findById(req.params.userId);
        res.json(user);
    } catch (err) {
        res.json({message: err});
    }
})

module.exports = router;