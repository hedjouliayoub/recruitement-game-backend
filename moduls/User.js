const mongoose = require('mongoose');

// Creating User Schema
const userSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    role: {
        type: String,
        require: true,
    },
    age: Number,
    date: String,
});

module.exports = mongoose.model('Users', userSchema);